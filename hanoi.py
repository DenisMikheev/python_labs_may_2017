import argparse


class Hanoi:
    def __init__(self, number):
        if number > 0:
            self.number = number
        else:
            raise ValueError('Invalid number of discs')
        self.pegA = list(reversed(range(number)))
        self.pegB = []
        self.pegC = []
        self.Verbose = False
        self.contTurns = 0
        self.contDepth = 0

    def display(self):
        print '-----------------'
        self.prettyprint(self.pegA, 0)
        self.prettyprint(self.pegB, 1)
        self.prettyprint(self.pegC, 2)
        print '-----------------'

    def prettyprint(self, peg, num):
        s = num.__str__() + ':'
        for p in peg:
            s += ' ' + (p + 1).__str__()
        print s

    def solve(self):
        self.display()
        self.solveRec(self.number, self.pegA, self.pegB, self.pegC)
        if not self.Verbose:
            self.display()

    def solveRec(self, n, source, helper, target):
        if n > 0:
            if self.number - n > self.contDepth:
                self.contDepth = self.number - n
            # move tover of size n-1 to helper
            self.solveRec(n - 1, source, target, helper)
            # move disk from source peg to target peg
            self.move(source, target)
            self.solveRec(n - 1, helper, source, target)

    def move(self, source, target):
        if source:
            target.append(source.pop())
            self.contTurns += 1  # count turns
            if self.Verbose:
                han1.display()


parser = argparse.ArgumentParser()
parser.add_argument("number", type=int)
parser.add_argument("-v", "--verbose", action="store_true")
args = parser.parse_args()

if args.number:
    han1 = Hanoi(args.number)
    if args.verbose:
        han1.Verbose = True
    han1.solve()
    print 'Turns: ' + han1.contTurns.__str__()
    print 'Depth: ' + han1.contDepth.__str__()
