#! /usr/bin/env python
import math
import cmath
import sys

countArgv = False
checkArgv = True
# ???????? ???-??
if len(sys.argv) != 4:
    print 'Nevernoe kol-vo argumentov!'
else:
    countArgv = True
    # ???????? ???????????? ?????????
    try:
        a = float(sys.argv[1])
        b = float(sys.argv[2])
        c = float(sys.argv[3])
    except:
        print 'Nekorrektnye argumenty!'
        checkArgv = False

if countArgv and checkArgv:
    print 'Vvedeno uravnenie:'
    print a.__str__() + '*x^2 + ' + b.__str__() + '*x + ' + c.__str__() + ' = 0'

    if a == 0 and b == 0:
        if c == 0:
            print '0 == 0, x - luboe chislo.'
        else:
            print c.__str__() + ' != 0, resheniy uravneniya net!'
    else:
        d = b * b - 4 * a * c
        print 'Discriminant = ' + d.__str__()
        if d < 0:
            print 'Naydeno dva mnimyx kornya: x1 = ' + ((-b + cmath.sqrt(d)) / (2 * a)).__str__()
            print '                           x2 = ' + ((-b - cmath.sqrt(d)) / (2 * a)).__str__()
        elif d == 0:
            print 'Nayden edinstvenny koren: x1 = ' + (-b / (2 * a)).__str__()
        elif d > 0:
            print 'Naydeno dva deystvitelnyx kornya: x1 = ' + ((-b + math.sqrt(d)) / (2 * a)).__str__()
            print '                    x2 = ' + ((-b - math.sqrt(d)) / (2 * a)).__str__()
